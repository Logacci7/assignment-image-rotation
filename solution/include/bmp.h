#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_H

#include "../include/inner_format.h"
#include <inttypes.h>
#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum close_status close_bmp (FILE* file);

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum read_status from_bmp( FILE * in, struct image * image);

enum write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_DATA_ERROR,
    WRITE_ERROR
};

enum write_status to_bmp ( FILE * out, struct image const * image);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_H
