#ifndef ASSIGNMENT_IMAGE_ROTATION_INNER_FORMAT_H
#define ASSIGNMENT_IMAGE_ROTATION_INNER_FORMAT_H
#include <inttypes.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t r,g,b;
};
#pragma pack(pop)

struct image{
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(const uint64_t width, const uint64_t height);
void destroy_image(struct image image);

#endif //ASSIGNMENT_IMAGE_ROTATION_INNER_FORMAT_H
