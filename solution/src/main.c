#include "../include/inner_format.h"              //change "/" to "../"
#include "../include/bmp.h"
#include "../include/transform_rotate.h"
#include <malloc.h>
#include <stdio.h>

int main(int argc, char ** argv){
    if (argc != 3){
        printf("Not enough args. Bye.");
        return 1;
    }

    FILE* in = fopen(argv[1],"rb");
    FILE* out = fopen(argv[2],"wb");;

    if (!in || !out){
        if (!in) close_bmp(in);
        else close_bmp(out);
        printf("Fail in file opening.\n");
        return OPEN_ERROR;
    }

    struct image image = {0};
    enum read_status read_in = from_bmp(in, &image);
    if (read_in != READ_OK){
        printf("Fail in file reading.\n");
        return read_in;
    }

    struct image rotate_img = rotate(image);

    enum write_status write_out = to_bmp(out, &rotate_img);
    if (write_out != WRITE_OK){
        printf("Fail in file writing.\n");

        close_bmp(in);
        close_bmp(out);
        destroy_image(image);
        destroy_image(rotate_img);

        return write_out;
    }


    if (fclose(in) || fclose(out)){
        printf("Fail in file closing.");

        destroy_image(image);
        destroy_image(rotate_img);

        return CLOSE_ERROR;
    }

    destroy_image(image);
    destroy_image(rotate_img);

    printf("Image rotated.");

    return 0;
}
