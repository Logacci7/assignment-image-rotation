#include "../include/inner_format.h"
#include <malloc.h>

struct image image_create(const uint64_t width, const uint64_t height){
    return (struct image) { .width = width, .height = height, .data = (struct pixel*) malloc(sizeof(struct pixel) * height * width)
    };
}

void destroy_image(struct image image){
    free(image.data);
}
