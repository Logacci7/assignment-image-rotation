#include "../include/bmp.h"
#include <malloc.h>

static uint64_t width;
static uint64_t height;

static uint64_t get_new_img_addr(uint64_t x_coord, uint64_t y_coord){
    return y_coord + x_coord * height;
}

static uint64_t get_old_img_addr(uint64_t x_coord, uint64_t y_coord){
    return (height - y_coord - 1) * width + x_coord;
}

struct image rotate( struct image const image ){

    width = image.width;
    height = image.height;

    struct image new_img = image_create(height, width);

    for (uint64_t i = 0; i < image.width; i++){
        for (uint64_t j = 0; j < image.height; j++)
            new_img.data[get_new_img_addr(i, j)] = image.data[get_old_img_addr(i, j)];
    }
    return new_img;
}
