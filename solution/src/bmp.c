#include "../include/bmp.h"
#include <malloc.h>
#include <stdio.h>

#define BMP_FORMAT 0x4D42
#define BMP_BIT_COUNT 0x18
#define BMP_HEADER_SIZE 0x28
#define BMP_OFF_SIZE 0x36

#pragma pack(push, 1)
struct bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

enum close_status close_bmp ( FILE* file ) {
    if (!fclose(file)) return CLOSE_OK;
    return CLOSE_ERROR;
}

static uint8_t get_padding( uint64_t width ){
    return 4  - (width * sizeof(struct pixel) % 4);
}

static enum read_status check_bmp_header( struct bmp_header const * header ){
    if (header->bfType != BMP_FORMAT) return READ_INVALID_SIGNATURE;
    if (header->biBitCount != BMP_BIT_COUNT) return READ_INVALID_BITS;
    if (header->bOffBits != BMP_OFF_SIZE) return READ_INVALID_HEADER;
    return READ_OK;
}

static enum read_status read_bmp_header( FILE* in, struct bmp_header* header ){
    if (fread(header, sizeof (struct bmp_header), 1, in) != 1) return READ_ERROR;
    return check_bmp_header(header);
}

static enum read_status read_bmp_data( FILE * in, struct image * image ){
    const uint64_t width = image->width;
    const uint64_t height = image->height;
    const uint8_t padding = get_padding(width);

    struct pixel* cur_pixels = image->data;

    for (size_t i = 0; i < height; i++){
        if (fread(cur_pixels, sizeof(struct pixel) * width, 1, in) != 1) return READ_ERROR;
        if (fseek(in, padding, SEEK_CUR)) return READ_ERROR;
        cur_pixels += width;
    }
    return READ_OK;
}

enum read_status from_bmp( FILE * in, struct image * image ){
    struct bmp_header header = {0};
    enum read_status header_read = read_bmp_header(in, &header);
    if (header_read != READ_OK)
        return header_read;

    *image = image_create(header.biWidth, header.biHeight);

    enum read_status pixel_read_code = read_bmp_data(in, image);

    if (pixel_read_code != READ_OK){
        destroy_image(*image);
        return pixel_read_code;
    }

    return READ_OK;
}

uint32_t get_img_size( const uint64_t width, const uint64_t height ){
    return (sizeof(struct pixel) * width + get_padding(width)) * height;
}

static struct bmp_header create_bmp_header( const uint64_t width, const uint64_t height ){
    uint32_t size = get_img_size( width, height);
    return (struct bmp_header) {
            .bfType = BMP_FORMAT,
            .bfileSize = size + BMP_HEADER_SIZE,
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_HEADER_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = BMP_BIT_COUNT,
            .biCompression = 0,
            .biSizeImage = size,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

static enum write_status write_bmp_header( FILE * out, struct bmp_header * header ){
    if (fwrite(header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_HEADER_ERROR;
    return WRITE_OK;
}

static enum write_status write_bmp_data( FILE * out, struct image const * image){
    const uint64_t width = image->width;
    const uint64_t height = image->height;
    const uint8_t padding = get_padding(width);

    struct pixel* data = image->data;

    for (size_t i = 0; i < height; i++){
        if (fwrite(data, sizeof(struct pixel) * width, 1, out) != 1) return WRITE_DATA_ERROR;
        if (fwrite(data, sizeof (char), padding, out ) != padding) return WRITE_DATA_ERROR;
        data += width;
    }
    return WRITE_OK;
}

enum write_status to_bmp ( FILE * out, struct image const * image ){
    struct bmp_header header = create_bmp_header(image->width, image->height);
    enum write_status header_write = write_bmp_header(out, &header);
    if (header_write != WRITE_OK) return header_write;
    return write_bmp_data(out, image);
}

